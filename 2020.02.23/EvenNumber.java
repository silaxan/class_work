public class EvenNumber{
	public static void main(String args[]){
		int sum = 0;
		int num = 10;
		for(int i = 0;i<=num;i++){
			if(i%2==0)
			sum = sum+i;
		}
		System.out.println("Even Number is "+ sum);
	}
}

//output
//Even Number is 30
