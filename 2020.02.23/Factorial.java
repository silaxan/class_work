public class Factorial{
	public static void main(String args[]){
		int sum = 1;
		for(int i = 1;i<=10;i++){
			sum = sum*i;
		}
		System.out.println("First 10 Factorial number is "+ sum);
	}
}

//output
//First 10 Factorial number is 3628800